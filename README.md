# Migrate Notejam to the cloud

## Description
In this project a python (Django) Notejam application has been deployed to AWS. CI/CD pipeline process creates both the application and its needed cloud resources. Any changes made by developers will launch the whole CI/CD process, containers will be updated and run in EKS without any downtime.


## Architecture
<img src="images/architecturev1.jpg" width=50%> 

### Key tools & cloud components used:
- [Terraform](https://www.terraform.io/) - Infrastructure as a Code.
- [Docker](https://www.docker.com/) - Containerization.
- [Amazon VPC](https://aws.amazon.com/vpc/) - Virtual isolated network.
- [Amazon EKS](https://aws.amazon.com/eks) - Managed Kubernetes cluster which provides a highly-available and fault-tolerant production-grade Kubernetes control plane.
- [Amazon ALB](https://aws.amazon.com/elasticloadbalancing/application-load-balancer/) - Load balancer.
- [Amazon RDS](https://aws.amazon.com/rds/) - Relational database. 


## Deploy
### - Deploy locally
### Prerequisites
- Docker

### Build, test and run locally
```bash
#clone
$ git clone https://gitlab.com/ilmavridis/notejam-cloud.git
$ cd notejam-cloud

#build
$ docker build -t notejam_project .

#test (optional)
$ docker run --env-file db_variables --rm notejam_project ./manage.py test

#run
docker run --env-file db_variables -p 8000:8000 --rm notejam_project

#or run in detached mode
$ docker run --env-file db_variables -p 8000:8000 -d notejam_project

```

Open [http://localhost:8000](http://localhost:8000) in your browser to access Notejam's ui.


### - Deploy to AWS
### Prerequisites
- Docker
- Terraform
- AWS account
- Gitlab account


### Infrastructure 
- Create an AWS S3 bucket for storing the state files and a DynamoDB table for locking. The state file of each workspace will be saved in a separate directory. 

```bash
#clone
$ git clone https://gitlab.com/ilmavridis/notejam-cloud.git
$ cd notejam-cloud/backend

$ terraform init

#e.g. create the backend infrastructure to store the state file of the development environment.
#There are similar tfvars files to use for each environment.
$ terraform plan -var-file="backend-development.tfvars"
$ terraform apply -var-file="backend-development.tfvars" --auto-approve

$ #terraform destroy -var-file="backend-development.tfvars" --auto-approve
```

- Now that the S3 bucket is ready, terraform can use it to store state files. 

```bash
$ cd ..

$ terraform init -backend-config="backend-development"

$ terraform plan -var-file="development.tfvars"
$ terraform apply  -var-file="development.tfvars" --auto-approve

$ terraform destroy -var-file="development.tfvars" --auto-approve
```



## CICD Pipeline
### Stages:
1. Build docker image
     - Deploy Docker in Docker to build and push the docker image to the Gitlab Registry.
2. Test docker image
     - Deploy Docker in Docker to run and test the container image.
3. Tag Docker image production ready
     - The image is marked with the production tag.
4. Plan infrastructure.
     - Terraform plan
5. Apply infrastructure.
     - Terraform apply
6. Destroy infrastructure.
     - Terraform destroy 

This demo follows a very simple branching strategy which is **not** suitable for production ready systems. A realistic production use case will include stages such as testing code in a development workspace, deploying a stage/pre-production environment, and implementing wider testing. 
The image below shows a production quality branch strategy based on the GitFlow branching model.


<img src="images/branches.jpg" width=50%> 

### Run
* A developer has to create a feature branch from the main branch and later submit the changes.

```bash
$ git checkout -b feature_branch
$ ##
$ git commit -am "My feature is ready"
$ git push origin feature_branch
```
* The build and push pipeline will be executed after the commit of the developer. Once the pipeline is complete, the developer can create a merge to the main request. 

<img src="images/buildpush.jpg">


* With this merge request, the test container pipeline will start running. The assignee can review the merge and test results and approve the merge in main.

<img src="images/merge.jpg">

* Next, the container to production pipeline must be initiated manually.

<img src="images/dockerproduction.jpg">

* Next, the container is tagged as ready for production, one must manually initiate the deployment of this container in the production environment. Terraform will create the infrastructure and run the container in AWS using Kubernetes.

<img src="images/terrainit.jpg">

* To destroy the infrastructure, the Destroy IaC pipeline must be manually initiated.



## License
[APACHE 2.0](https://www.apache.org/licenses/LICENSE-2.0)
