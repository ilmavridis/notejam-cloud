terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.73.0"
    }
  }
}



# Configure the AWS Provider
provider "aws" {
  region = var.region
}


#S3 for storing tfstate. Key, region and dynamodb_table values are in the backend-{enviroment} file
terraform {
  backend "s3" {
    bucket = "terraform-state-notejam"
  }
}



#VPC
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.11.4"

  name = "${var.vpc_name}-${var.environment}-vpc"
  cidr = var.vpc_cidr

  azs             = var.vpc_availability_zones
  public_subnets  = var.vpc_public_subnets_cidr
  private_subnets = var.vpc_private_subnets_cidr


  #A Nat gateway per VPC
  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false


  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    Environment                                 = "${var.environment}"
  }

  #Tag VPC subnets so Kubernetes can discover them
  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                    = "1"
    Environment                                 = "${var.environment}"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = "1"
    Environment                                 = "${var.environment}"
  }

}


#EKS - Kubernetes
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.2.3"


  cluster_name                    = "${var.eks_cluster_name}-${var.environment}-cluster"
  cluster_version                 = var.eks_cluster_version
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  node_security_group_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }

  vpc_id = module.vpc.vpc_id

  subnet_ids = module.vpc.private_subnets


  eks_managed_node_groups = {
    eks_ng = {
      min_size     = var.min_nodes
      max_size     = var.max_nodes
      desired_size = var.desired_nodes

      instance_types = "${var.eks_node_group_instance_types_DNS}"

      tags = {
        Environment  = "${var.environment}"
      }
    }

  }

  tags = {
    Environment = "${var.environment}"
  }
}


data "tls_certificate" "auth" {
  url = module.eks.cluster_oidc_issuer_url
}


resource "aws_iam_openid_connect_provider" "main" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.auth.certificates[0].sha1_fingerprint]
  url             = data.aws_eks_cluster.default.identity[0].oidc[0].issuer
}


data "aws_eks_cluster" "default" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "default" {
  name = module.eks.cluster_id
}


#Kubernetes
provider "kubernetes" {
  host                   = data.aws_eks_cluster.default.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.default.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.default.token
}



resource "local_file" "kubeconfig" {
  sensitive_content = templatefile("kubeconfig.tpl", {
    cluster_name = var.cluster_name,
    clusterca    = data.aws_eks_cluster.default.certificate_authority[0].data,
    endpoint     = data.aws_eks_cluster.default.endpoint,
  })
  filename = "./kubeconfig-${var.cluster_name}"
}




#ALB
data "aws_caller_identity" "default" {}

resource "aws_iam_policy" "ALB-policy" {
  name   = "ALBIngressControllerIAMPolicy"
  policy = file("iam-policy-alb.json") #https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.8/docs/examples/iam-policy.json
}


resource "aws_iam_role" "eks_alb_ingress_controller" {
  name        = "eks-alb-ingress-controller"
  description = "Permissions required by the Kubernetes AWS ALB Ingress controller to do its job."

  force_detach_policies = true

  assume_role_policy = <<ROLE
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::${data.aws_caller_identity.default.account_id}:oidc-provider/${replace(data.aws_eks_cluster.default.identity[0].oidc[0].issuer, "https://", "")}"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "${replace(data.aws_eks_cluster.default.identity[0].oidc[0].issuer, "https://", "")}:sub": "system:serviceaccount:kube-system:alb-ingress-controller" 
        }
      }
    }
  ]
}
ROLE
}


resource "aws_iam_role_policy_attachment" "ALB-policy_attachment" {
  policy_arn = aws_iam_policy.ALB-policy.arn
  role       = aws_iam_role.eks_alb_ingress_controller.name
}



resource "kubernetes_service_account" "ingress" {
  automount_service_account_token = true
  metadata {
    name      = "alb-ingress-controller"
    namespace = "kube-system"
    labels = {
      "app.kubernetes.io/name"       = "alb-ingress-controller"
      "app.kubernetes.io/managed-by" = "terraform"
    }
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.eks_alb_ingress_controller.arn
    }
  }
}


resource "kubernetes_cluster_role" "ingress" {
  metadata {
    name = "alb-ingress-controller"
    labels = {
      "app.kubernetes.io/name"       = "alb-ingress-controller"
      "app.kubernetes.io/managed-by" = "terraform"
    }
  }

  rule {
    api_groups = ["", "extensions"]
    resources  = ["configmaps", "endpoints", "events", "ingresses", "ingresses/status", "services"]
    verbs      = ["create", "get", "list", "update", "watch", "patch"]
  }

  rule {
    api_groups = ["", "extensions"]
    resources  = ["nodes", "pods", "secrets", "services", "namespaces"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "ingress" {
  metadata {
    name = "alb-ingress-controller"
    labels = {
      "app.kubernetes.io/name"       = "alb-ingress-controller"
      "app.kubernetes.io/managed-by" = "terraform"
    }
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.ingress.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.ingress.metadata[0].name
    namespace = kubernetes_service_account.ingress.metadata[0].namespace
  }

  depends_on = [kubernetes_cluster_role.ingress]
}


data "aws_vpc" "selected" {
  id = module.vpc.vpc_id
}


resource "kubernetes_deployment" "ingress" {
  metadata {
    name      = "alb-ingress-controller"
    namespace = "kube-system"
    labels = {
      "app.kubernetes.io/name"       = "alb-ingress-controller"
      "app.kubernetes.io/version"    = "v1.1.6"
      "app.kubernetes.io/managed-by" = "terraform"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        "app.kubernetes.io/name" = "alb-ingress-controller"
      }
    }

    template {
      metadata {
        labels = {
          "app.kubernetes.io/name"    = "alb-ingress-controller"
          "app.kubernetes.io/version" = "v1.1.6"
        }
      }

      spec {
        dns_policy                       = "ClusterFirst"
        restart_policy                   = "Always"
        service_account_name             = kubernetes_service_account.ingress.metadata[0].name
        termination_grace_period_seconds = 60

        container {
          name              = "alb-ingress-controller"
          image             = "docker.io/amazon/aws-alb-ingress-controller:v1.1.6"
          image_pull_policy = "Always"

          args = [
            "--ingress-class=alb",
            "--cluster-name=${var.cluster_name}",
            "--aws-vpc-id=${data.aws_vpc.selected.id}",
            "--aws-region=${var.region}",
            "--aws-max-retries=10",
          ]
          volume_mount {
            mount_path = "/var/run/secrets/kubernetes.io/serviceaccount"
            name       = kubernetes_service_account.ingress.default_secret_name
            read_only  = true
          }
        }
        volume {
          name = kubernetes_service_account.ingress.default_secret_name

          secret {
            secret_name = kubernetes_service_account.ingress.default_secret_name
          }
        }
      }
    }
  }

  depends_on = [kubernetes_cluster_role_binding.ingress]
}





#Notejam
resource "kubernetes_namespace" "namespace" {
  metadata {
    name = var.kubernetes_namespace
    labels = {
      app = "Notejam"
    }
  }
}


resource "kubernetes_deployment" "notejam" {
  metadata {
    name      = "${var.deployment_name}-${terraform.workspace}"
    namespace = kubernetes_namespace.namespace.metadata[0].name
    labels = {
      app = "Notejam"
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "Notejam"
      }
    }

    template {
      metadata {
        labels = {
          app = "Notejam"
        }
      }

      spec {
        security_context { #pod securityContext
          fs_group = 12222
        }
        container {
          image = "registry.gitlab.com/ilmavridis/notejam-cloud:${var.environment}"
          name  = "notejam"
          security_context { #container securityContext
            run_as_user                = 12222
            run_as_group               = 12222
            privileged                 = false
            allow_privilege_escalation = false
          }
          image_pull_policy = "Always"
          env {
            name  = "DB_ENGINE"
            value = "django.db.backends.${var.rds_engine}ql_psycopg2" #add ql_psycopg2 in the end to form the appropriate value for django db backend
          }
          env {
            name  = "DB_NAME"
            value = var.rds_name
          }
          env {
            name  = "DB_USER"
            value = var.rds_user
          }
          env {
            name  = "DB_PASS"
            value = var.rds_pass
          }
          env {
            name  = "DB_HOST"
            value = aws_db_instance.default.address
          }
          env {
            name  = "DB_PORT"
            value = var.rds_port
          }
          /*resources {
            limits = {
              cpu    = "1"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }*/
          port {
            name           = "example"
            container_port = 8000
          }
        }
      }
    }
  }
  depends_on = [kubernetes_namespace.namespace]
}


resource "kubernetes_service" "notejam" {
  metadata {
    name      = "${var.deployment_name}-service"
    namespace = kubernetes_namespace.namespace.metadata[0].name
    labels = {
      app = "Notejam"
    }
  }
  spec {
    selector = {
      app = "Notejam"
    }
    port {
      port        = 8000
      target_port = 8000
      protocol    = "TCP"
    }

    type = "NodePort"
  }
  depends_on = [kubernetes_deployment.notejam]
}


resource "kubernetes_ingress" "notejam" {
  metadata {
    name      = "${var.deployment_name}-ingress"
    namespace = kubernetes_namespace.namespace.metadata[0].name
    annotations = {
      "kubernetes.io/ingress.class"           = "alb"
      "alb.ingress.kubernetes.io/scheme"      = "internet-facing"
      "alb.ingress.kubernetes.io/target-type" = "ip"
    }
    labels = {
      app = "Notejam"
    }
  }

  spec {
    backend {
      service_name = kubernetes_service.notejam.metadata[0].name
      service_port = kubernetes_service.notejam.spec[0].port[0].port
    }
    rule {
      http {
        path {
          path = "/*"
          backend {
            service_name = kubernetes_service.notejam.metadata[0].name
            service_port = kubernetes_service.notejam.spec[0].port[0].port
          }
        }
      }
    }
  }
  depends_on = [kubernetes_service.notejam]
}





#RDS
#Allow traffic from EKS instance into the RDS database.
resource "aws_security_group" "sg_rds" {
  name = "postgresql-${var.environment}"
  vpc_id = module.vpc.vpc_id

  ingress {
    description = "Allow only EKS to connect"
    from_port   = var.rds_port
    to_port     = var.rds_port
    protocol    = "tcp"
    cidr_blocks =  var.vpc_private_subnets_cidr
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg_rds_${terraform.workspace}"
  }
}


resource "aws_db_subnet_group" "default" {
  name       = "${var.rds_subnet_group_name}-${terraform.workspace}"
  subnet_ids = module.vpc.private_subnets
  tags = {
    Name        = "RDS_SG_${terraform.workspace}"
    Environment = terraform.workspace
  }
}



resource "aws_db_instance" "default" {
  identifier             = "notejam-${terraform.workspace}"
  allocated_storage      = var.rds_storage_size
  storage_type           = var.rds_storage_type
  engine                 = var.rds_engine
  engine_version         = var.rds_engine_version
  instance_class         = var.rds_instance_class
  name                   = var.rds_name
  username               = var.rds_user
  password               = var.rds_pass
  db_subnet_group_name   = aws_db_subnet_group.default.name
  publicly_accessible    = false
  skip_final_snapshot    = true
  multi_az               = true ##Standby replica in another az
  vpc_security_group_ids = [aws_security_group.sg_rds.id]
  depends_on             = [aws_db_subnet_group.default]
}


