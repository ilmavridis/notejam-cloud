region        = "eu-central-1"
tfstate_table = "development-imavrdis-tfstate-lock" ##Unique
environment   = "development"



#VPC
cluster_name             = "eks"
vpc_cidr                 = "10.0.0.0/16"
vpc_name                 = "main"
vpc_availability_zones   = ["eu-central-1a", "eu-central-1b"]
vpc_public_subnets_cidr  = ["10.0.0.0/24", "10.0.1.0/24"]
vpc_private_subnets_cidr = ["10.0.3.0/24", "10.0.4.0/24"]


#EKS - Fargate - Kubernetes
eks_cluster_name                  = "eks"
eks_cluster_version               = "1.21" #kubernetes version
eks_node_group_instance_types     = ["t2.medium"]
min_nodes                         = 2
max_nodes                         = 4
desired_nodes                     = 3
eks_node_group_instance_types_DNS = ["t2.micro"]


#RDS
rds_port              = "5432"
rds_name              = "notejam"
rds_storage_type      = "gp2"
deployment_name       = "notejam"
rds_subnet_group_name = "rds-subnet"
rds_user              = "notejamuser"
rds_pass              = "passsNoIlMav"
rds_storage_size      = "10"
rds_engine            = "postgres"
rds_engine_version    = "14.1"
rds_instance_class    = "db.t3.micro"
kubernetes_namespace  = "nginx-ingress"
