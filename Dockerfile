FROM python:2.7-alpine

MAINTAINER Ilias Mavridis <il.mavridis@gmail.com>

ENV PYTHONUNBUFFERED 1

# Update pip
RUN apk update \
    && python -m pip install pip==20.3.4


#Create user and directory
RUN addgroup --gid 12222 notejam_group \
    && adduser -u 12222 -S notejam_user -G notejam_group \
    && mkdir /notejam \
    && chown -R notejam_user:notejam_group /notejam


RUN apk add postgresql-dev gcc python-dev musl-dev ##Required to support Postgres RDS

COPY ./notejam-django/requirements.txt ./requirements.txt
COPY ./notejam-django/notejam/ ./notejam


USER notejam_user


RUN pip install -r requirements.txt



RUN pip install psycopg2-binary ##Required to support Postgres RDS

WORKDIR /notejam

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
