variable "region" {
  description = "AWS region"
  type        = string
  default     = "eu-central-1"
}


variable "tfstate_table" {
  type        = string
  description = "DynamoDB table to use for locking"
}
