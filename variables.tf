variable "region" {
  description = "AWS region"
  type        = string
  default     = "eu-central-1"
}

variable "environment" {
  type        = string
  description = "Environment name"
}

variable "tfstate_table" {
  type        = string
  description = "DynamoDB table to use for locking"
}




#VPC
variable "cluster_name" {
  type        = string
  description = "Name of cluster"
}

variable "vpc_name" {
  description = "Name of vpc"
  type        = string
}

variable "vpc_cidr" {
  type        = string
  description = "Cidr value of vpc"
}

variable "vpc_availability_zones" {
  type        = list(string)
  description = "List of availability zones"
}

variable "vpc_public_subnets_cidr" {
  type        = list(string)
  description = "List of public cidr"
}

variable "vpc_private_subnets_cidr" {
  type        = list(string)
  description = "List of private cidr"
}



#EKS - Fargate - Kubernetes
variable "eks_cluster_name" {
  type        = string
  description = "Name of the eks cluster"
}

variable "eks_cluster_version" {
  type        = string
  description = "Version of the eks cluster"
}

variable "eks_node_group_instance_types" {
  type        = list(string) ### auto to thelei to modile na einai []
  description = "Instance type of node group"
}

variable "min_nodes" {
  type        = number
  description = "Minimum number of nodes"
}

variable "max_nodes" {
  type        = number
  description = "Maximum number of nodes"
}

variable "desired_nodes" {
  type        = number
  description = "Desired number of nodes"
}

variable "eks_node_group_instance_types_DNS" {
  type        = list(string)
  description = "Instance type of node group for running DNS"
}





##RDS
variable "rds_storage_type" {
  description = "Type of storage"
}

variable "rds_engine" {
  type        = string
  description = "RDS Database Engine Name"
}

variable "rds_engine_version" {
  type        = string
  description = "RDS Database Engine Version"
}

variable "rds_instance_class" {
  type        = string
  description = "RDS Database Instance type/class"
}

variable "kubernetes_namespace" {
  type        = string
  description = "Kubernetes namespace for selection"
}

variable "deployment_name" {
  type        = string
  description = "Name of the Deployment"
}

variable "rds_subnet_group_name" {
  type        = string
  description = "RDS Subnet Group Name"
}

variable "rds_user" {
  type        = string
  description = "RDS Database Username"
  sensitive   = true
}

variable "rds_pass" {
  type        = string
  description = "RDS Database Password"
  sensitive   = true
}

variable "rds_storage_size" {
  type        = string
  description = "RDS Database size"
}

variable "rds_name" {
  type        = string
  description = "Database Name"
  sensitive   = true
}

variable "rds_port" {
  type        = string
  description = "Database port"
}
