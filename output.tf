output "vpc_id" {
  value       = module.vpc.vpc_id
  description = "The default VPC ID"
}

output "availability_zones" {
  value       = module.vpc.azs
  description = "The availability zones"
}

output "private_subnets" {
  value       = module.vpc.private_subnets
  description = "The private subnet IDs"
}

output "public_subnets" {
  value       = module.vpc.public_subnets
  description = "The public subnet IDs"
}

output "db_instance_endpoint" {
  value       = aws_db_instance.default.endpoint
  description = "The db connection endpoint"
}

output "db_subnet_group_name" {
  value       = aws_db_subnet_group.default.name
  description = "The db subnet group name"
}
